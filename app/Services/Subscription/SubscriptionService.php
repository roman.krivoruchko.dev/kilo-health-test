<?php

namespace App\Services\Subscription;

use App\Models\Subscription;
use Nette\Utils\DateTime;

class SubscriptionService {

    /**
     * @var \Illuminate\Support\Collection
     */
    private $subscriptions;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptions = collect([
            new Subscription(['payment_id' => 'qwe111']),
            new Subscription(['payment_id' => 'qwe112']),
        ]);
    }

    /**
     * @param string $id
     * @return Subscription
     */
    public function getByPaymentData(string $id)
    {
        return $this->subscriptions->where('payment_id', $id)->first();
    }

    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Exception
     */
    public function activate(Subscription $subscription): Subscription
    {
        $subscription->active_date = (new DateTime())->modify('+1 month');
        // save here
        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Exception
     */
    public function deactivate(Subscription $subscription): Subscription
    {
        $subscription->active_date = (new DateTime());
        // save here
        return $subscription;

    }
}
