<?php

namespace App\Services\Payment\Contracts;

use App\Models\Subscription;

/**
 * Interface PaymentServiceContract
 */
interface PaymentServiceContract {

    public function buy(Subscription $subscription);

    public function renew(Subscription $subscription);

    public function failToRenew(Subscription $subscription);

    public function cancel(Subscription $subscription);
}
