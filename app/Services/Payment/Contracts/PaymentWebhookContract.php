<?php

namespace App\Services\Payment\Contracts;


/**
 * Interface PaymentServiceContract
 */
interface PaymentWebhookContract {

    const EVENT_BUY = 'buy';
    const EVENT_RENEW = 'renew';
    const EVENT_FAIL_TO_RENEW = 'failToRenew';
    const EVENT_CANCEL = 'cancel';

    public function getEventByRequest(array $data);

    public function getPaymentIdByRequest(array $data);
}
