<?php

namespace App\Services\Payment;


use App\Models\Subscription;
use App\Services\Payment\Contracts\PaymentServiceContract;
use App\Services\Payment\Contracts\PaymentWebhookContract;
use App\Services\Subscription\SubscriptionService;

class ApplePaymentService implements PaymentServiceContract, PaymentWebhookContract {


    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * ApplePaymentService constructor.
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Exception
     */
    public function buy(Subscription $subscription) : Subscription {
        $subscription = $this->subscriptionService->activate($subscription);
        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Exception
     */
    public function renew(Subscription $subscription) : Subscription {
        $subscription = $this->subscriptionService->activate($subscription);
        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Exception
     */
    public function failToRenew(Subscription $subscription) : Subscription {
        $subscription = $this->subscriptionService->deactivate($subscription);
        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Exception
     */
    public function cancel(Subscription $subscription) : Subscription {
        $subscription = $this->subscriptionService->deactivate($subscription);
        return $subscription;
    }

    /**
     * @param array $data
     * @return mixed|null
     */
    public function getEventByRequest(array $data) {
        return self::$eventList[$data['notification_type']] ?? null;
    }

    /**
     * @param array $data
     * @return mixed|null
     */
    public function getPaymentIdByRequest(array $data) {
        return $data['auto_renew_adam_id'] ?? null;
    }

    /**
     * @var array
     */
    private static $eventList = [
        'INITIAL_BUY' => PaymentWebhookContract::EVENT_BUY,
        'DID_RENEW' => PaymentWebhookContract::EVENT_RENEW,
        'DID_FAIL_TO_RENEW' => PaymentWebhookContract::EVENT_FAIL_TO_RENEW,
        'CANCEL' => PaymentWebhookContract::EVENT_CANCEL,
    ];

}
