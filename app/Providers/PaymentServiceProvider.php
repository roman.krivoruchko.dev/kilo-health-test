<?php

namespace App\Providers;

use App\Exceptions\Payment\UnsupportedPaymentSystem;
use Illuminate\Support\ServiceProvider;
use App\Services\Payment\Contracts\PaymentServiceContract;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentServiceContract::class, function() {
            $paymentProviders = config('payments.providers');
            if (in_array($this->app->request->paymentSystem, array_keys($paymentProviders))) {
                return $this->app->make($paymentProviders[$this->app->request->paymentSystem]);
            } else {
                throw new UnsupportedPaymentSystem('Unsupported payment system');
            }
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
