<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Services\Payment\Contracts\PaymentServiceContract;
use App\Services\Payment\Contracts\PaymentWebhookContract;
use App\Services\Subscription\SubscriptionService;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    /**
     * @var PaymentServiceContract
     */
    private $paymentService;
    /**
     * @var UserService
     */
    private $subscriptionService;


    /**
     * WebhookController constructor.
     * @param PaymentServiceContract|PaymentWebhookContract $paymentService
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(
        PaymentServiceContract $paymentService,
        SubscriptionService $subscriptionService
    )
    {
        $this->paymentService = $paymentService;
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @param Request $request
     * @return void|null
     */
    public function webhook(Request $request)
    {
        $requestData = $request->all();
        $paymentSystemEvent = $this->paymentService->getEventByRequest($requestData);

        $subscription = $this->subscriptionService->getByPaymentData(
            $this->paymentService->getPaymentIdByRequest($requestData)
        );

        if (!$paymentSystemEvent || !$subscription) {
            Log::error('Bad webhook request', $request->all());
            return; // code 200 no need retries
        }

        $result = null; // TODO: remove - just to see the result during development

        switch ($paymentSystemEvent) {
            case PaymentWebhookContract::EVENT_BUY:
                $result = $this->paymentService->buy($subscription);
                // notify user or other logic
                break;
            case PaymentWebhookContract::EVENT_RENEW:
                $result = $this->paymentService->renew($subscription);
                break;
            case PaymentWebhookContract::EVENT_FAIL_TO_RENEW:
                $result = $this->paymentService->failToRenew($subscription);
                break;
            case PaymentWebhookContract::EVENT_CANCEL:
                $result = $this->paymentService->cancel($subscription);
                break;
        }

        return $result; // code 200 no need retries
    }
}
