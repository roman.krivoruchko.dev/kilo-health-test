<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class WebhookAuthenticate
{
    const WEBHOOK_PASSWORD = 'temp_pass';

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws AuthorizationException
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->get('password') === self::WEBHOOK_PASSWORD) {
            return $next($request);
        } else {
            throw new AuthorizationException();
        }
    }
}
